import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainPageComponent } from './main-page/main-page.component';
import { EditProjectComponent } from './projects/components/edit-project/edit-project.component';
import { ProjectsComponent } from './projects/components/projects/projects.component';
import { UnsavedChangesGuard } from './services/unsaved-changes-guard.service';
import { TasksComponent } from './tasks/tasks/tasks.component';
import { TeamsComponent } from './teams/teams/teams.component';
import { UsersComponent } from './users/users/users.component';


const routes: Routes = [
  {path: '', component: MainPageComponent},
  { path: 'tasks', component: TasksComponent },
  { path: 'projects', component: ProjectsComponent },
  { path: 'projects/:id', component: EditProjectComponent, canDeactivate: [UnsavedChangesGuard]},
  { path: 'teams', component: TeamsComponent },
  { path: 'users', component: UsersComponent },
  { path: '**', redirectTo: ''} 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
