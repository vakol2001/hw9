import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ProjectsModule } from './projects/projects.module';
import { TasksModule } from './tasks/tasks.module';
import { TeamsModule } from './teams/teams.module';
import { UsersModule } from './users/users.module';
import { AppRoutingModule } from './app-routing.module';
import { MainPageComponent } from './main-page/main-page.component';
import { HttpClientModule } from '@angular/common/http';
import { UnsavedChangesGuard } from './services/unsaved-changes-guard.service';

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent
  ],
  imports: [
    BrowserModule,
    ProjectsModule,
    TasksModule,
    UsersModule,
    TeamsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    UnsavedChangesGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
