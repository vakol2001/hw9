import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customDate'
})
export class CustomDatePipe implements PipeTransform {

  transform(value: Date | string | undefined): string {
    if (!value) {
      return "-";
    }
    const date = new Date(value);
    const months = ['січня', 'лютого', 'березня', 'квітня', 'травня', 'червня', 'липня', 'серпня', 'вересня', 'жовтня', 'листопада', 'грудня'];

    return date.getDate() + ' ' + months[date.getMonth()] + ' ' + date.getFullYear();
  }

}
