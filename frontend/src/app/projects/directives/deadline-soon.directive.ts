import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import { ProjectModel } from '../models/ProjectModel';

@Directive({
  selector: '[deadlineSoon]'
})
export class DeadlineSoonDirective implements OnInit {

  @Input('deadlineSoon') input: ProjectModel | undefined;

  private project: ProjectModel | undefined;

  constructor(private el: ElementRef) {

   }
  ngOnInit(): void {
    this.project = this.input;
    if(this.project?.deadline.getMonth() == new Date().getMonth()){
      this.el.nativeElement.style.backgroundColor = 'red';
    }
  }

}
