import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PorjectRowComponent } from './porject-row.component';

describe('PorjectRowComponent', () => {
  let component: PorjectRowComponent;
  let fixture: ComponentFixture<PorjectRowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PorjectRowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PorjectRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
