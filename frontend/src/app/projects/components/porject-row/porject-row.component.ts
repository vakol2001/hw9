import { Component, Input, OnInit } from '@angular/core';
import { ProjectModel } from '../../models/ProjectModel';

@Component({
  selector: 'porject-row',
  templateUrl: './porject-row.component.html',
  styleUrls: ['./porject-row.component.scss']
})
export class PorjectRowComponent implements OnInit {

  @Input() public project: ProjectModel | undefined;

  constructor() { }

  ngOnInit(): void {
  }

}
