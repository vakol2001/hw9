import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ProjectModel } from '../../models/ProjectModel';
import { UpdateProjectModel } from '../../models/UpdateProjectModel';
import { ProjectService } from '../../services/project.service';

@Component({
  selector: 'app-edit-project',
  templateUrl: './edit-project.component.html',
  styleUrls: ['./edit-project.component.scss']
})
export class EditProjectComponent implements OnInit, OnDestroy {

  startProject: ProjectModel | undefined;

  project : ProjectModel | undefined;

  canDeactivate(): boolean{
    let unsaved: boolean;
    if (!this.project && !this.startProject) {
      unsaved = true;
    } else if (!this.project || !this.startProject) {
      unsaved = false;
    } else {
      unsaved = !this.projectEqual(this.project, this.startProject)
    }
    if (unsaved) {
      return window.confirm("You have unsaved changes. Continue?");
    }
    return true;
    
  }

  projectEqual(project1: ProjectModel, project2: ProjectModel) {
    return project1.id === project2.id &&
           project1.authorId === project2.authorId &&
           project1.deadline.toString() === project2.deadline.toString() &&
           project1.description === project2.description &&
           project1.name === project2.name &&
           project1.teamId === project2.teamId;
  }

  saveResult: 'saved' | 'error' | 'nothing' = 'nothing' ;
  message: string = '';
  private unsubscribe$ = new Subject<void>();

  constructor(
    private route: ActivatedRoute,
    private projectsService: ProjectService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getProject();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getProject(): void{
    const id = parseInt(this.route.snapshot.paramMap.get('id')!, 10);
    this.projectsService.getProject(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(resp => {
        if (resp.body) {
          this.project = resp.body;
          this.project.deadline = new Date(resp.body.deadline);
          this.project.createdAt = new Date(resp.body.createdAt);

          this.cloneProject();
        }
      });
  }

  private cloneProject() {
    if (this.project) {
      this.startProject = {
        id: this.project.id,
        authorId: this.project.authorId,
        teamId: this.project.teamId,
        name: this.project.name,
        description: this.project.description,
        deadline: this.project.deadline,
        createdAt: this.project.createdAt
      };
    }
    else
    {
      this.startProject = this.project;
    }

  }

  saveProject(): void {
    if (!this.project) {
        this.saveResult = 'saved';
      return;
    }
    const updateProject : UpdateProjectModel = {
      id: this.project.id,
      name: this.project.name,
      description: this.project.description,
      deadline: this.project.deadline,
      authorId: this.project.authorId,
      teamId: this.project.teamId
    }
    this.projectsService.updateProject(updateProject)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(resp => {
        if (resp.ok) {
          this.saveResult = 'saved';
          this.message = 'Project have saved';
          this.cloneProject();
        }
        else{
          this.saveResult = 'error';
          this.message = JSON.stringify(resp.body);
        }
        },
        error => {
          this.saveResult = 'error';
          this.message = 'status = ' + JSON.stringify(error.status);
        })
  }

  deleteProject(){
    this.projectsService.deleteProject(this.project!.id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(resp => {
        if (resp.ok) {
          this.router.navigate(['/projects']);
        } else {
          this.saveResult = 'error';
          this.message = JSON.stringify(resp.body);
        }
      },
      error => {
        this.saveResult = 'error';
        this.message = 'status = ' + JSON.stringify(error.status);
      })
  }

}
