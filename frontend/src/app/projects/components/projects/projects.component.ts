import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ProjectModel } from '../../models/ProjectModel';
import { ProjectService } from '../../services/project.service';

@Component({
  selector: 'projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit, OnDestroy {
  
  loadingProjects: boolean = false;
  projects: ProjectModel[]  | undefined;

  private unsubscribe$ = new Subject<void>();

  
  constructor(private projectService: ProjectService) { }
  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  ngOnInit(): void {
    this.getPosts();
  }

  public getPosts() {
    this.loadingProjects = true;
    this.projectService
        .getProjects()
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
            (resp) => {
                this.loadingProjects = false;
                this.projects = resp.body?.map(proj => {
                  proj.createdAt = new Date(proj.createdAt);
                  proj.deadline = new Date(proj.deadline);
                  return proj;
                 } );
            },
            (error) => (this.loadingProjects = false)
        );
}

}
