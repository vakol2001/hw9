export interface UpdateProjectModel {
    id: number;
    authorId: number;
    teamId: number;
    name: string;
    description: string;
    deadline: Date;
}