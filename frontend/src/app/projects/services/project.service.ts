import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { ProjectModel } from '../models/ProjectModel';
import { UpdateProjectModel } from '../models/UpdateProjectModel';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  public routePrefix = 'api/projects';

  constructor(private httpService: HttpService) { }

  public getProjects() {
    return this.httpService.getFullRequest<ProjectModel[]>(this.routePrefix);
  }

  public getProject(id: number) {
    return this.httpService.getFullRequest<ProjectModel>(`${this.routePrefix}/${id}`);
  }

  public updateProject(project: UpdateProjectModel) {
    return this.httpService.postFullRequest<UpdateProjectModel>(this.routePrefix, project);
  }

  public deleteProject(id: number) {
    return this.httpService.deleteFullRequest<void>(`${this.routePrefix}/${id}`);
  }
}
