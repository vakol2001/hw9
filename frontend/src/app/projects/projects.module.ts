import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectsComponent } from './components/projects/projects.component';
import { AppRoutingModule } from '../app-routing.module';
import { PorjectRowComponent } from './components/porject-row/porject-row.component';
import { EditProjectComponent } from './components/edit-project/edit-project.component';
import { FormsModule } from '@angular/forms';
import { PipesModule } from '../pipes/pipes.module';
import { DeadlineSoonDirective } from './directives/deadline-soon.directive';



@NgModule({
  declarations: [
    ProjectsComponent,
    PorjectRowComponent,
    EditProjectComponent,
    DeadlineSoonDirective
  ],
  imports: [
    AppRoutingModule,
    CommonModule,
    FormsModule,
    PipesModule
  ],
  exports: [
    ProjectsComponent,
    EditProjectComponent
  ]
})
export class ProjectsModule { }
