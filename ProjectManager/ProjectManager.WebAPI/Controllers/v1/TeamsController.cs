﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using ProjectManager.Application.Teams.Commands;
using ProjectManager.Application.Teams.Models;
using ProjectManager.Application.Teams.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectManager.WebAPI.Controllers.v1
{
    public class TeamsController : ApiController
    {
        public TeamsController(IMediator mediator) : base(mediator) { }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamModel>>> GetAll()
        {
            return Ok(await Mediator.Send(new GetAllTeamsQuery()));
        }
        [HttpGet("{Id}")]
        public async Task<ActionResult<TeamModel>> GetById([FromRoute] GetTeamByIdQuery query)
        {
            try
            {
                return Ok(await Mediator.Send(query));
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }

        }
        [HttpPost]
        public async Task<ActionResult<TeamModel>> Update([FromBody] UpdateTeamCommand command)
        {
            try
            {
                return Ok(await Mediator.Send(command));
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
        }
        [HttpPut]
        public async Task<ActionResult<TeamModel>> Put([FromBody] CreateTeamCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpDelete("{Id}")]
        public async Task<ActionResult> Delete([FromRoute] DeleteTeamCommand command)
        {
            try
            {
                await Mediator.Send(command);
                return NoContent();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }

        }
    }
}
