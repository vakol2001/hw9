﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectManager.Application.LinqQueries.Models;
using ProjectManager.Application.LinqQueries.Queries;
using ProjectManager.Application.Projects.Queries;
using ProjectManager.Application.Tasks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectManager.WebAPI.Controllers.v1
{
    public class LinqQueriesController : ApiController
    {
        public LinqQueriesController(IMediator mediator) : base(mediator) { }

        
        [HttpGet("ProjectInfoByUserId/{Id}")] // 1
        public async Task<ActionResult<IDictionary<string, int>>> GetProjectInfoByUserId([FromRoute] GetProjectInfoByUserIdQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet("TasksWithShortNameByUserId/{Id}")] // 2
        public async Task<ActionResult<IEnumerable<TaskModel>>> GetTasksWithShortNameByUserId([FromRoute] GetTasksWithShortNameByUserIdQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet("TasksWichFinishedInCurrentYearByUserId/{Id}")] // 3
        public async Task<ActionResult<IEnumerable<TaskNameModel>>> GetTasksWichFinishedInCurrentYearByUserId([FromRoute] GetTasksWichFinishedInCurrentYearByUserIdQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet("GetTeamsWithParticipantsOlderThan10Query")] // 4
        public async Task<ActionResult<IEnumerable<TeamParticipantsModel>>> GetTeamsWithParticipantsOlderThan10()
        {
            return Ok(await Mediator.Send(new GetTeamsWithParticipantsOlderThan10Query()));
        }

        [HttpGet("SortedUsersWithSortedTasks")] // 5
        public async Task<ActionResult<IEnumerable<TeamParticipantsModel>>> GetSortedUsersWithSortedTasks()
        {
            return Ok(await Mediator.Send(new GetSortedUsersWithSortedTasksQuery()));
        }

        [HttpGet("UserInfoById/{Id}")] // 6
        public async Task<ActionResult<IEnumerable<TaskNameModel>>> GetUserInfoById([FromRoute] GetUserInfoByIdQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet("ProjectsInfo")] // 7
        public async Task<ActionResult<IEnumerable<TeamParticipantsModel>>> GetProjectsInfo()
        {
            return Ok(await Mediator.Send(new GetProjectsInfoQuery()));
        }

    }
}
