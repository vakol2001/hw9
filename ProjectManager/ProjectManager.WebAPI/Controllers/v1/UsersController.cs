﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using ProjectManager.Application.Users.Commands;
using ProjectManager.Application.Users.Models;
using ProjectManager.Application.Users.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectManager.WebAPI.Controllers.v1
{
    public class UsersController : ApiController
    {
        public UsersController(IMediator mediator) : base(mediator) { }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserModel>>> GetAll()
        {
            return Ok(await Mediator.Send(new GetAllUsersQuery()));
        }
        [HttpGet("{Id}")]
        public async Task<ActionResult<UserModel>> GetById([FromRoute] GetUserByIdQuery query)
        {
            try
            {
                return Ok(await Mediator.Send(query));
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }

        }
        [HttpPost]
        public async Task<ActionResult<UserModel>> Update([FromBody] UpdateUserCommand command)
        {
            try
            {
                return Ok(await Mediator.Send(command));
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
        }
        [HttpPut]
        public async Task<ActionResult<UserModel>> Put([FromBody] CreateUserCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpDelete("{Id}")]
        public async Task<ActionResult> Delete([FromRoute] DeleteUserCommand command)
        {
            try
            {
                await Mediator.Send(command);
                return NoContent();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }

        }
    }
}
