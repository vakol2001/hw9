﻿using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace ProjectManager.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class ApiController : ControllerBase
    {
        protected ApiController(IMediator mediator) => Mediator = mediator;

        public IMediator Mediator { get; }
    }
}
