﻿using Newtonsoft.Json;
using System;

namespace ProjectManager.Data.Entities
{
    public abstract class Entity
    {
        [JsonIgnore]
        public int Id { get; set; }
        [JsonIgnore]
        public DateTime CreatedAt { get; set; }
        [JsonIgnore]
        public DateTime UpdatedAt { get; set; }
    }
}
