﻿using System;
using System.Collections.Generic;

namespace ProjectManager.Data.Entities
{
    public class User : Entity
    {
        public int? TeamId { get; set; }
        public virtual Team Team { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime BirthDay { get; set; }

        public virtual ICollection<Task> Tasks { get; set; } = new HashSet<Task>();
        public virtual ICollection<Project> Projects { get; set; } = new HashSet<Project>();
    }
}
