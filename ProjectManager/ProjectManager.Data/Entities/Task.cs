﻿using ProjectManager.Data.Enums;
using System;

namespace ProjectManager.Data.Entities
{
    public class Task : Entity
    {
        public int ProjectId { get; set; }
        public virtual Project Project { get; set; }

        public int PerformerId { get; set; }
        public virtual User Performer { get; set; } 

        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
