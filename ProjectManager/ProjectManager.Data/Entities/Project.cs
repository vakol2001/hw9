﻿using System;
using System.Collections.Generic;

namespace ProjectManager.Data.Entities
{
    public class Project : Entity
    {
        public int AuthorId { get; set; }
        public virtual User Author { get; set; }

        public int TeamId { get; set; }
        public virtual Team Team { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }

        public virtual ICollection<Task> Tasks { get; set; } = new HashSet<Task>();
    }
}
