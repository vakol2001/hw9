﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectManager.Data.Entities;

namespace ProjectManager.Data.Configurations
{
    class TeamConfiguration : IEntityTypeConfiguration<Team>
    {
        public void Configure(EntityTypeBuilder<Team> builder)
        {
            builder.Property(x => x.Id) // Id
                   .ValueGeneratedOnAdd()
                   .UseIdentityColumn(seed: 0, increment: 1);

            builder.Property(p => p.Name) // Name
                   .IsRequired()
                   .HasMaxLength(50);


            builder.HasIndex(p => p.Name);
        }
    }
}
