﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectManager.Data.Entities;

namespace ProjectManager.Data.Configurations
{
    class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {

            builder.Property(x => x.Id) // Id
                   .ValueGeneratedOnAdd()
                   .UseIdentityColumn(seed: 0, increment: 1);

            builder.HasOne(u => u.Team) // TeamId
                   .WithMany(t => t.Participants)
                   .HasForeignKey(u => u.TeamId)
                   .OnDelete(DeleteBehavior.SetNull);

            builder.Property(u => u.FirstName) // FirstName
                   .HasMaxLength(50);

            builder.Property(u => u.LastName) // LastName
                   .IsRequired()
                   .HasMaxLength(50);

            builder.Property(u => u.Email) // Email
                   .HasMaxLength(50);


            builder.HasIndex(u => u.LastName);

            builder.HasIndex(u => u.FirstName)
                   .HasFilter("[FirstName] IS NOT NULL");

        }
    }
}
