﻿namespace ProjectManager.Data.Enums
{
    public enum TaskState
    {
        ToDo,
        InProgress,
        Done,
        Canceled
    }
}
