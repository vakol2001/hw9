﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace ProjectManager.BL
{
    static class CancellationTokenCheckExtentions
    {
        internal static IEnumerable<T> CheckCanceled<T>(this IEnumerable<T> collection, CancellationToken cancellationToken)
        {
            return collection.Select(x =>
            {
                cancellationToken.ThrowIfCancellationRequested();
                return x;
            });
        }

        internal static ParallelQuery<T> CheckCanceled<T>(this ParallelQuery<T> collection, CancellationToken cancellationToken)
        {
            return collection.Select(x =>
            {
                cancellationToken.ThrowIfCancellationRequested();
                return x;
            });
        }
    }
}
