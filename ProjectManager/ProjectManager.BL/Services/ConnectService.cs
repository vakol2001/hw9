﻿using Newtonsoft.Json;
using ProjectManager.BL.Entities;
using ProjectManager.BL.Interfaces;
using ProjectManager.BL.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.BL.Services
{
    public class ConnectService : IConnectService
    {
        private readonly HttpClient _client;

        public ConnectService(string connectionString = "https://bsa21.azurewebsites.net/api/")
        {
            _client = new HttpClient
            {
                BaseAddress = new Uri(connectionString)
            };
        }

        private bool _disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }
            if (disposing)
            {
                _client?.Dispose();
            }
            _disposed = true;
        }

        public async Task<Project> GetProjectAsync(int id, CancellationToken cancellationToken = default)
        {
            return JsonConvert.DeserializeObject<Project>(await _client.GetStringAsync($"projects/{id}", cancellationToken));
        }

        public async Task<IEnumerable<Project>> GetProjectsAsync(CancellationToken cancellationToken = default)
        {
            return JsonConvert.DeserializeObject<IEnumerable<Project>>(await _client.GetStringAsync($"projects", cancellationToken));
        }

        public async Task<Entities.Task> GetTaskAsync(int id, CancellationToken cancellationToken = default)
        {
            return JsonConvert.DeserializeObject<Entities.Task>(await _client.GetStringAsync($"tasks/{id}", cancellationToken));
        }

        public async Task<IEnumerable<Entities.Task>> GetTasksAsync(CancellationToken cancellationToken = default)
        {
            return JsonConvert.DeserializeObject<IEnumerable<Entities.Task>>(await _client.GetStringAsync($"tasks", cancellationToken));
        }

        public async Task<Team> GetTeamAsync(int id, CancellationToken cancellationToken = default)
        {
            return JsonConvert.DeserializeObject<Team>(await _client.GetStringAsync($"teams/{id}", cancellationToken));
        }

        public async Task<IEnumerable<Team>> GetTeamsAsync(CancellationToken cancellationToken = default)
        {
            return JsonConvert.DeserializeObject<IEnumerable<Team>>(await _client.GetStringAsync($"teams", cancellationToken));
        }

        public async Task<User> GetUserAsync(int id, CancellationToken cancellationToken = default)
        {
            return JsonConvert.DeserializeObject<User>(await _client.GetStringAsync($"users/{id}", cancellationToken));
        }

        public async Task<IEnumerable<User>> GetUsersAsync(CancellationToken cancellationToken = default)
        {
            return JsonConvert.DeserializeObject<IEnumerable<User>>(await _client.GetStringAsync($"users", cancellationToken));
        }

        public async System.Threading.Tasks.Task PostTaskAsync(UpdateTaskModel model, CancellationToken cancellationToken = default)
        {
            var response = await _client.PostAsJsonAsync("tasks", model, cancellationToken);
            response.EnsureSuccessStatusCode();
        }
    }
}
