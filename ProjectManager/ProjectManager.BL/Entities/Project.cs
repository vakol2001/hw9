﻿using System;
using System.Collections.Generic;

namespace ProjectManager.BL.Entities
{
    public record Project
    {
        public int Id { get; init; }

        public int AuthorId { get; init; }
        public User Author { get; init; }

        public int TeamId { get; init; }
        public Team Team { get; init; }

        public string Name { get; init; }
        public string Description { get; init; }
        public DateTime Deadline { get; init; }
        public DateTime CreatedAt { get; init; }

        public IEnumerable<Task> Tasks { get; init; }

        public override string ToString()
        {
            return $"Project # {Id} '{Name}'";
        }
    }
}
