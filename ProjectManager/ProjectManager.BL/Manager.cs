﻿using ProjectManager.BL.Entities;
using ProjectManager.BL.Interfaces;
using ProjectManager.BL.Models;
using ProjectManager.BL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;
using TaskProject = ProjectManager.BL.Entities.Task;

namespace ProjectManager.BL
{
    public class Manager
    {
        IEnumerable<Project> _projects;
        private readonly IConnectService _connectService;

        public Manager(IConnectService connectService)
        {
            _connectService = connectService;
        }
        public async Task DonwoadDataAsync()
        {
            var usersTask = _connectService.GetUsersAsync();
            var projectsTask = _connectService.GetProjectsAsync();
            var tasksTask = _connectService.GetTasksAsync();
            var teamsTask = _connectService.GetTeamsAsync();

            var users = await usersTask;
            var projects = await projectsTask;
            var tasks = await tasksTask;
            var teams = await teamsTask;



            tasks = tasks.Join(inner: users,
                               outerKeySelector: t => t.PerformerId,
                               innerKeySelector: u => u.Id,
                               resultSelector: (t, u) => t with { Performer = u });

            _projects = projects.GroupJoin(
                                    inner: tasks,
                                    outerKeySelector: p => p.Id,
                                    innerKeySelector: t => t.ProjectId,
                                    resultSelector: (p, ts) => p with { Tasks = ts.ToList() })
                                .Join(
                                    inner: users,
                                    outerKeySelector: p => p.AuthorId,
                                    innerKeySelector: u => u.Id,
                                    resultSelector: (p, u) => p with { Author = u })
                                .Join(
                                    inner: teams,
                                    outerKeySelector: p => p.TeamId,
                                    innerKeySelector: t => t.Id,
                                    resultSelector: (p, t) => p with { Team = t })
                                .ToList();
        }

        public Dictionary<Project, int> GetProjectInfoByUserId(int id) // task 1
        {

            return _projects.Where(p => p.AuthorId == id)
                            .ToDictionary(
                                  keySelector: p => p,
                                  elementSelector: p => p.Tasks.Count());
        }

        public Task<Dictionary<Project, int>> GetProjectInfoByUserIdAsync(int id, CancellationToken cancellationToken = default) // async task 1
        {

            return Task.Run(() => _projects.AsParallel()
                            .Where(p => p.AuthorId == id)
                            .CheckCanceled(cancellationToken)
                            .ToDictionary(
                                  keySelector: p => p,
                                  elementSelector: p => p.Tasks.Count()),
                            cancellationToken);
        }

        public IEnumerable<TaskProject> GetTasksWithShortNameByUserId(int id) // task 2
        {

            return _projects.SelectMany(p => p.Tasks)
                            .Where(t => t.PerformerId == id)
                            .Where(t => t.Name.Length < 45);
        }

        public async Task<IEnumerable<TaskProject>> GetTasksWithShortNameByUserIdAsync(int id, CancellationToken cancellationToken = default) // async task 2
        {

            return await Task.Run(() => _projects.AsParallel()
                                .CheckCanceled(cancellationToken)
                                .SelectMany(p => p.Tasks)
                                .Where(t => t.PerformerId == id)
                                .Where(t => t.Name.Length < 45)
                                .ToList(),
                            cancellationToken);
        }

        public IEnumerable<TaskModel> GetTasksWichFinishedInCurrentYearByUserId(int id) // task 3
        {

            return _projects.SelectMany(p => p.Tasks)
                            .Where(t => t.PerformerId == id)
                            .Where(t => t.FinishedAt?.Year == DateTime.Now.Year)
                            .Select(t => new TaskModel { Id = t.Id, Name = t.Name });
        }

        public async Task<IEnumerable<TaskModel>> GetTasksWichFinishedInCurrentYearByUserIdAsync(int id, CancellationToken cancellationToken = default) // async task 3
        {

            return await Task.Run(() => _projects
                                .AsParallel()
                                .CheckCanceled(cancellationToken)
                                .SelectMany(p => p.Tasks)
                                .Where(t => t.PerformerId == id)
                                .Where(t => t.FinishedAt?.Year == DateTime.Now.Year)
                                .Select(t => new TaskModel { Id = t.Id, Name = t.Name })
                                .ToList(),
                            cancellationToken);
        }

        public IEnumerable<TeamModel> GetTeamsWithParticipantsOlderThan10() // task 4
        {
            return _projects.Select(p => p.Team)
                            .Distinct()
                            .GroupJoin(
                                inner: _projects.SelectMany(p => p.Tasks)
                                                .Select(t => t.Performer)
                                                .Concat(_projects.Select(p => p.Author))
                                                .Distinct(),
                                outerKeySelector: t => t.Id,
                                innerKeySelector: u => u.TeamId,
                                resultSelector: (t, us) => t with { Participants = us })
                            .Where(t => t.Participants.All(u => DateTime.Now.Year - u.BirthDay.Year > 10))
                            .Select(t => new TeamModel
                            {
                                Id = t.Id,
                                Name = t.Name,
                                Participants = t.Participants.OrderByDescending(u => u.RegisteredAt)
                            });
        }

        public async Task<IEnumerable<TeamModel>> GetTeamsWithParticipantsOlderThan10Async(CancellationToken cancellationToken = default) // async task 4
        {
            return await Task.Run(() => _projects
                                .AsParallel()
                                .CheckCanceled(cancellationToken)
                                .Select(p => p.Team)
                                .Distinct()
                                .GroupJoin(
                                    inner: _projects.AsParallel()
                                                    .CheckCanceled(cancellationToken)
                                                    .SelectMany(p => p.Tasks)
                                                    .Select(t => t.Performer)
                                                    .Concat(_projects.AsParallel().CheckCanceled(cancellationToken).Select(p => p.Author))
                                                    .Distinct(),
                                    outerKeySelector: t => t.Id,
                                    innerKeySelector: u => u.TeamId,
                                    resultSelector: (t, us) => t with { Participants = us.ToList() })
                                .Where(t => t.Participants.All(u => DateTime.Now.Year - u.BirthDay.Year > 10))
                                .Select(t => new TeamModel
                                {
                                    Id = t.Id,
                                    Name = t.Name,
                                    Participants = t.Participants.OrderByDescending(u => u.RegisteredAt)
                                })
                                .ToList(),
                            cancellationToken);
        }

        public IEnumerable<User> GetSortedUsersWithSortedTasks() // task 5
        {

            return _projects.SelectMany(p => p.Tasks)
                            .Distinct()
                            .GroupBy(t => t.Performer)
                            .Select(g => g.Key with { Tasks = g.OrderByDescending(t => t.Name.Length) })
                            .OrderBy(u => u.FirstName);
        }

        public async Task<IEnumerable<User>> GetSortedUsersWithSortedTasksAsync(CancellationToken cancellationToken = default) // async task 5
        {

            return await Task.Run(() => _projects
                                .AsParallel()
                                .CheckCanceled(cancellationToken)
                                .SelectMany(p => p.Tasks)
                                .Distinct()
                                .GroupBy(t => t.Performer)
                                .Select(g => g.Key with
                                {
                                    Tasks = g.AsParallel()
                                                                   .CheckCanceled(cancellationToken)
                                                                   .OrderByDescending(t => t.Name.Length)
                                                                   .ToList()
                                })
                                .OrderBy(u => u.FirstName)
                                .ToList(),
                            cancellationToken);
        }

        public UserModel GetUserInfo(int id) // task 6
        {

            return _projects.Where(p => p.AuthorId == id)
                            .OrderBy(p => p.CreatedAt)
                            .Select(p => new UserModel
                            {
                                User = p.Author,
                                LastProject = p,
                                LastProjectAmountTasks = p.Tasks.Count(),
                                AmountNotFinishedTasks = _projects.SelectMany(p => p.Tasks)
                                                                    .Count(t => t.PerformerId == p.AuthorId && t.FinishedAt is null),
                                LongestTask = _projects.SelectMany(p => p.Tasks)
                                                        .Where(t => t.Id == id)
                                                        .OrderByDescending(t => (t.FinishedAt ?? DateTime.Now) - t.CreatedAt)
                                                        .FirstOrDefault()
                            })
                            .LastOrDefault();
        }

        public Task<UserModel> GetUserInfoAsync(int id, CancellationToken cancellationToken = default) // async task 6
        {

            return Task.Run(() => _projects
                                .AsParallel()
                                .CheckCanceled(cancellationToken)
                                .Where(p => p.AuthorId == id)
                                .OrderBy(p => p.CreatedAt)
                                .Select(p => new UserModel
                                {
                                    User = p.Author,
                                    LastProject = p,
                                    LastProjectAmountTasks = p.Tasks.Count(),
                                    AmountNotFinishedTasks = _projects.AsParallel()
                                                                      .CheckCanceled(cancellationToken)
                                                                      .SelectMany(p => p.Tasks)
                                                                      .Count(t => t.PerformerId == p.AuthorId && t.FinishedAt.HasValue),
                                    LongestTask = _projects.AsParallel()
                                                           .CheckCanceled(cancellationToken)
                                                           .SelectMany(p => p.Tasks)
                                                           .Where(t => t.Id == id)
                                                           .OrderByDescending(t => (t.FinishedAt ?? DateTime.Now) - t.CreatedAt)
                                                           .FirstOrDefault()
                                })
                                .LastOrDefault(),
                            cancellationToken);
        }

        public IEnumerable<ProjectModel> GetProjectsInfo() // task 7
        {

            return _projects.Where(p => p.Description.Length > 20 || p.Tasks.Count() < 3)
                            .Select(p => new ProjectModel
                            {
                                Project = p,
                                LongestTaskByDescription = p.Tasks.OrderBy(t => t.Description.Length).LastOrDefault(),
                                ShortestTaskByName = p.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault(),
                                ParticipantsAmount = _projects.SelectMany(pr => pr.Tasks)
                                                              .Select(t => t.Performer)
                                                              .Concat(_projects.Select(pr => pr.Author))
                                                              .Distinct()
                                                              .Count(u => u.TeamId == p.TeamId)
                            });
        }

        public async Task<IEnumerable<ProjectModel>> GetProjectsInfoAsync(CancellationToken cancellationToken = default) // async task 7
        {

            return await Task.Run(() => _projects
                                .AsParallel()
                                .CheckCanceled(cancellationToken)
                                .Where(p => p.Description.Length > 20 || p.Tasks.Count() < 3)
                                .Select(p => new ProjectModel
                                {
                                    Project = p,
                                    LongestTaskByDescription = p.Tasks
                                        .AsParallel()
                                        .CheckCanceled(cancellationToken)
                                        .OrderBy(t => t.Description.Length)
                                        .LastOrDefault(),
                                    ShortestTaskByName = p.Tasks
                                        .AsParallel()
                                        .CheckCanceled(cancellationToken)
                                        .OrderBy(t => t.Name.Length)
                                        .FirstOrDefault(),
                                    ParticipantsAmount = _projects
                                        .AsParallel()
                                        .CheckCanceled(cancellationToken)
                                        .SelectMany(pr => pr.Tasks)
                                        .Select(t => t.Performer)
                                        .Concat(_projects.AsParallel().CheckCanceled(cancellationToken).Select(pr => pr.Author))
                                        .Distinct()
                                        .Count(u => u.TeamId == p.TeamId)
                                })
                                .ToList(),
                            cancellationToken);
        }

        public Task<IEnumerable<int>> MarkRandomTaskWithDelay(int delay, CancellationToken cancellationToken = default)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                return Task.FromResult(Enumerable.Empty<int>());
            }

            System.Timers.Timer timer = new(delay);

            var source = new TaskCompletionSource<IEnumerable<int>>();
            var ids = new List<int>();
            var random = new Random();
            timer.Elapsed += async (s, e) =>
            {
                var tasks = (await _connectService.GetTasksAsync()).Where(t => !t.FinishedAt.HasValue).ToList();
                if (!tasks.Any())
                {
                    timer.Stop();
                    source.SetResult(ids);
                }

                var randomTask = tasks.ElementAt(random.Next(tasks.Count));


                var model = new UpdateTaskModel
                {
                    Id = randomTask.Id,
                    Description = randomTask.Description,
                    Name = randomTask.Name,
                    PerformerId = randomTask.PerformerId,
                    ProjectId = randomTask.ProjectId,
                    State = randomTask.State,
                    FinishedAt = DateTime.Now
                };
                try
                {
                    await _connectService.PostTaskAsync(model, cancellationToken);
                }
                catch(OperationCanceledException ex)
                {
                    Console.WriteLine(ex.Message);
                }

                ids.Add(model.Id);

                if (cancellationToken.IsCancellationRequested)
                {
                    timer.Stop();
                    source.SetResult(ids);
                }
            };

            timer.Start();
            return source.Task;
        }

    }

}