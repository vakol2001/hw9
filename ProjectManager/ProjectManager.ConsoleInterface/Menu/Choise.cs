﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.ConsoleInterface.Menu
{
    class Choise
    {
        public event EventHandler WasChosen;

        public void Invoke()
        {
            WasChosen?.Invoke(this, EventArgs.Empty);
        }
    }
}
