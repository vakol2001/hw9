﻿using System;
using System.Collections.Generic;

namespace ProjectManager.ConsoleInterface.Menu
{
    class MenuWorker
    {

        private readonly Dictionary<string, Choise> _choises = new();
        private readonly string _exit;

        public Choise this[string index]
        {
            get
            {
                if (index == _exit)
                {
                    throw new ArgumentException("Index cannot match output combination", nameof(index));
                }
                if (_choises.ContainsKey(index))
                {
                    return _choises[index];
                }
                var choise = new Choise();
                _choises[index] = choise;
                return choise;
            }
        }
        public Action PrintMenu { get; set; }
        public MenuWorker(string exit = "0")
        {
            _exit = exit;
        }
        public void Start()
        {
            do
            {
                Console.WriteLine("Choose a menu item.");
                PrintMenu?.Invoke();
                var input = Console.ReadLine();
                if (input == _exit)
                {
                    break;
                }
                if (_choises.ContainsKey(input))
                {
                    _choises[input].Invoke();
                }
                else
                {
                    Console.WriteLine("No action is reserved for such a combination.");
                }
            } while (true);
        }
    }
}
