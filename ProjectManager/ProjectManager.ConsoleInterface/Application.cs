﻿using ProjectManager.BL;
using ProjectManager.BL.Services;
using ProjectManager.ConsoleInterface.Menu;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.ConsoleInterface
{
    sealed class Application
    {
        readonly Manager _manager;
        readonly MenuWorker _menuWorker = new();
        CancellationTokenSource _cancellationTokenSource = new();
        public Application(string connectionString)
        {
            _manager = new Manager(new ConnectService(connectionString));
            _menuWorker.PrintMenu = PrintMenu;

            _menuWorker["1"].WasChosen += async (_, _) => await Wrapper(_manager.GetProjectInfoByUserIdAsync);
            _menuWorker["2"].WasChosen += async (_, _) => await Wrapper(_manager.GetTasksWithShortNameByUserIdAsync);
            _menuWorker["3"].WasChosen += async (_, _) => await Wrapper(_manager.GetTasksWichFinishedInCurrentYearByUserIdAsync);
            _menuWorker["4"].WasChosen += async (_, _) => await Wrapper(_manager.GetTeamsWithParticipantsOlderThan10Async);
            _menuWorker["5"].WasChosen += async (_, _) => await Wrapper(_manager.GetSortedUsersWithSortedTasksAsync);
            _menuWorker["6"].WasChosen += async (_, _) => await Wrapper(_manager.GetUserInfoAsync);
            _menuWorker["7"].WasChosen += async (_, _) => await Wrapper(_manager.GetProjectsInfoAsync);
            _menuWorker["8"].WasChosen += async (_, _) =>
            {
                Console.WriteLine("Start markering");
                var ids = await _manager.MarkRandomTaskWithDelay(1000, _cancellationTokenSource.Token);
                Console.WriteLine("Finish markering");
                Console.WriteLine("Markered tasks : [" + string.Join(',', ids) + ']');

            };

            _menuWorker["C"].WasChosen += (_, _) =>
            {
                _cancellationTokenSource.Cancel();
                _cancellationTokenSource = new CancellationTokenSource();
            };

            _menuWorker["U"].WasChosen += async (_, _) =>
            {
                _cancellationTokenSource.Cancel();
                _cancellationTokenSource = new CancellationTokenSource();
                await _manager.DonwoadDataAsync();
                Console.WriteLine("Finish updating");
            };

        }

        public Task SetUpAsync() => _manager.DonwoadDataAsync();
        public void Start() => _menuWorker.Start();

        private void PrintMenu()
        {
            Console.WriteLine("1 -\tGetProjectInfoByUserId\t\t\t\ttask1");
            Console.WriteLine("2 -\tGetTasksWithShortNameByUserId\t\t\ttask2");
            Console.WriteLine("3 -\tGetTasksWichFinishedInCurrentYearByUserId\ttask3");
            Console.WriteLine("4 -\tGetTeamsWithParticipantsOlderThan10\t\ttask4");
            Console.WriteLine("5 -\tGetSortedUsersWithSortedTasks\t\t\ttask5");
            Console.WriteLine("6 -\tGetUserInfo\t\t\t\t\ttask6");
            Console.WriteLine("7 -\tGetProjectsInfo\t\t\t\t\ttask7");
            Console.WriteLine("8 -\tMarkRandomTaskWithDelay 1000");
            Console.WriteLine("C -\tCancel all");
            Console.WriteLine("U -\tUpdate data\t\tThis will cancel any pending action!");
            Console.WriteLine("0 -\tExit");
        }


        private async Task Wrapper<T>(Func<CancellationToken, Task<T>> action)
        {
            try
            {
                JsonPrinter.PrintObject(await action(_cancellationTokenSource.Token));
            }
            catch (OperationCanceledException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private async Task Wrapper<T>(Func<int, CancellationToken, Task<T>> action)
        {
            try
            {
                JsonPrinter.PrintObject(await action(GetId(), _cancellationTokenSource.Token));
            }
            catch (OperationCanceledException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static int GetId()
        {
            Console.WriteLine("Enter id");
            int id = int.Parse(Console.ReadLine());
            return id;
        }
    }
}
