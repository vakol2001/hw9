using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using ProjectManager.Application.LinqQueries.Queries;
using ProjectManager.Application.Projects.Commands;
using ProjectManager.Application.Projects.Models;
using ProjectManager.Application.Tasks.Commands;
using ProjectManager.Application.Tasks.Models;
using ProjectManager.Application.Teams.Commands;
using ProjectManager.Application.Teams.Models;
using ProjectManager.Data;
using ProjectManager.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Xunit;

namespace ProjectManager.WebAPI.IntegrationTests
{
    public class Tests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {

        private readonly CustomWebApplicationFactory<Startup> _factory;

        public Tests(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task PutProject_WhenAddNewProject_ThenGetProject()
        {
            var client = _factory.CreateClient();
            var request = new
            {
                Name = "TestName",
                AuthorId = 1,
                Deadline = new DateTime(2021, 12, 12),
                Description = "TestDescription",
                TeamId = 1
            };
            var response = await client.PutAsJsonAsync("/api/projects", request);

            response.EnsureSuccessStatusCode();

            var stringResponse = await response.Content.ReadAsStringAsync();

            var projectModel = JsonConvert.DeserializeObject<ProjectModel>(stringResponse);


            var gettedProject = await client.GetFromJsonAsync<ProjectModel>($"/api/projects/{projectModel.Id}");

            Assert.Equal(request.Name, gettedProject.Name);
            Assert.Equal(request.AuthorId, gettedProject.AuthorId);
            Assert.Equal(request.Deadline, gettedProject.Deadline);
            Assert.Equal(request.Description, gettedProject.Description);
            Assert.Equal(request.TeamId, gettedProject.TeamId);

            await client.DeleteAsync($"/api/projects/{projectModel.Id}");
        }

        [Fact]
        public async Task PutProject_WhenAddNewProject_ThenCheckResponse()
        {
            var client = _factory.CreateClient();
            var request = new
            {
                Name = "TestName",
                AuthorId = 1,
                Deadline = new DateTime(2021, 12, 12),
                Description = "TestDescription",
                TeamId = 1
            };
            var response = await client.PutAsJsonAsync("/api/projects", request);

            response.EnsureSuccessStatusCode();

            var stringResponse = await response.Content.ReadAsStringAsync();

            var projectModel = JsonConvert.DeserializeObject<ProjectModel>(stringResponse);

            Assert.Equal(request.Name, projectModel.Name);
            Assert.Equal(request.AuthorId, projectModel.AuthorId);
            Assert.Equal(request.Deadline, projectModel.Deadline);
            Assert.Equal(request.Description, projectModel.Description);
            Assert.Equal(request.TeamId, projectModel.TeamId);

            await client.DeleteAsync($"/api/projects/{projectModel.Id}");
        }

        [Fact]
        public async Task DeleteUser_WhenUserNotExists_Then404StatusCode()
        {

            var client = _factory.CreateClient();

            var response = await client.DeleteAsync("/api/users/999");

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task DeleteUser_WhenDeleteUser_ThenNoUserOnGetQuery()
        {

            var client = _factory.CreateClient();

            var responseDelete = await client.DeleteAsync("/api/users/1");

            Assert.Equal(HttpStatusCode.NoContent, responseDelete.StatusCode);

            var responseGet = await client.GetAsync("/api/users/1");

            Assert.Equal(HttpStatusCode.NotFound, responseGet.StatusCode);

        }

        [Fact]
        public async Task PutTeam_WhenAddNewTeam_ThenGetTeam()
        {
            var client = _factory.CreateClient();
            var request = new
            {
                Name = "TestName"
            };
            var response = await client.PutAsJsonAsync("/api/teams", request);

            response.EnsureSuccessStatusCode();

            var stringResponse = await response.Content.ReadAsStringAsync();

            var teamModel = JsonConvert.DeserializeObject<TeamModel>(stringResponse);


            var gettedTeam = await client.GetFromJsonAsync<TeamModel>($"/api/teams/{teamModel.Id}");

            Assert.Equal(request.Name, gettedTeam.Name);

            await client.DeleteAsync($"/api/teams/{teamModel.Id}");
        }

        [Fact]
        public async Task PutTeam_WhenAddNewTeam_ThenCheckResponse()
        {
            var client = _factory.CreateClient();
            var request = new
            {
                Name = "TestName"
            };
            var response = await client.PutAsJsonAsync("/api/teams", request);

            response.EnsureSuccessStatusCode();

            var stringResponse = await response.Content.ReadAsStringAsync();

            var teamModel = JsonConvert.DeserializeObject<TeamModel>(stringResponse);

            Assert.Equal(request.Name, teamModel.Name);

            await client.DeleteAsync($"/api/teams/{teamModel.Id}");
        }

        [Theory]
        [InlineData(1, 0)]
        [InlineData(2, 0)]
        [InlineData(22, 1)]
        [InlineData(29, 2)]
        public async Task GetProjectInfoByUserIdQuery_WhenNProjectsForUser_ThenNProjects(int id, int count)
        {
            var client = _factory.CreateClient();

            var response = await client.GetFromJsonAsync<IDictionary<string, int>>($"/api/LinqQueries/ProjectInfoByUserId/{id}");

            Assert.Equal(count, response.Count);

        }

        [Fact]
        public async Task DeleteTask_WhenTaskNotExists_Then404StatusCode()
        {
            var client = _factory.CreateClient();

            var response = await client.DeleteAsync("/api/users/9999");

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task DeleteTask_WhenDeleteTask_ThenNoTaskOnGetQuery()
        {

            var client = _factory.CreateClient();

            var responseDelete = await client.DeleteAsync("/api/tasks/1");

            Assert.Equal(HttpStatusCode.NoContent, responseDelete.StatusCode);

            var responseGet = await client.GetAsync("/api/tasks/1");

            Assert.Equal(HttpStatusCode.NotFound, responseGet.StatusCode);

        }


        [Theory]
        [InlineData(2,0)]
        [InlineData(22,5)]
        [InlineData(999,0)]
        public async Task GetUnfinishedTasksByUserIdQuery_WhenNTask_ThenNTasks(int userId, int count)
        {
            var client = _factory.CreateClient();

            var response = await client.GetFromJsonAsync<IEnumerable<TaskModel>>($"/api/tasks/unfinished/{userId}");

            Assert.Equal(count, response.Count());
        }

        [Fact]
        public async Task GetUnfinishedTasksByUserIdQuery_WhentaskUpdateToFinished_ThenTasksMinusOne()
        {
            var client = _factory.CreateClient();

            var response = await client.GetFromJsonAsync<IEnumerable<TaskModel>>($"/api/tasks/unfinished/23");

            Assert.Equal(6, response.Count());

            var task = response.First();
            var request = new 
            {
                task.Id,
                task.ProjectId,
                task.PerformerId,
                task.Name,
                task.Description,
                task.State,
                FinishedAt = DateTime.Now
            };

            await client.PostAsJsonAsync("/api/tasks", request);

            var secondResponse = await client.GetFromJsonAsync<IEnumerable<TaskModel>>($"/api/tasks/unfinished/23");

            Assert.Equal(5, secondResponse.Count());
        }
    }
}
