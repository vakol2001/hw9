﻿using MediatR;
using ProjectManager.Data;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Users.Commands
{
    public class DeleteUserCommand : IRequest
    {
        public int Id { get; set; }

        public class Handler : IRequestHandler<DeleteUserCommand>
        {
            private readonly ProjectManagerContext _context;

            public Handler(ProjectManagerContext context)
            {
                _context = context;
            }


            public async Task<Unit> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
            {
                var user = await _context.Users.FindAsync(new object[] { request.Id }, cancellationToken) ?? throw new KeyNotFoundException();
                _context.Users.Remove(user);
                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }

        }
    }
}
