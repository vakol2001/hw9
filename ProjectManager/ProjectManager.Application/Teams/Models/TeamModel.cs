﻿using System;

namespace ProjectManager.Application.Teams.Models
{
    public class TeamModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
