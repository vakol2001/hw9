﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Teams.Models;
using ProjectManager.Data;
using ProjectManager.Data.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Teams.Commands
{
    public class CreateTeamCommand : IRequest<TeamModel>
    {
        public string Name { get; set; }


        public class Handler : IRequestHandler<CreateTeamCommand, TeamModel>
        {
            private readonly ProjectManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, ProjectManagerContext context)
            {
                _context = context;
                _mapper = mapper;
            }


            public async Task<TeamModel> Handle(CreateTeamCommand request, CancellationToken cancellationToken)
            {
                var teamToAdd = _mapper.Map<Team>(request);
                var team = await _context.Teams.AddAsync(teamToAdd, cancellationToken);
                var mappedTeam = _mapper.Map<TeamModel>(team.Entity);
                await _context.SaveChangesAsync(cancellationToken);
                return mappedTeam;
            }
        }
    }
}
