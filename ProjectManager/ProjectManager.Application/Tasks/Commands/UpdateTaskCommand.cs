﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Tasks.Models;
using ProjectManager.Data;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Task = ProjectManager.Data.Entities.Task;

namespace ProjectManager.Application.Tasks.Commands
{
    public class UpdateTaskCommand : IRequest<TaskModel>
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int State { get; set; }
        public DateTime? FinishedAt { get; set; }


        public class Handler : IRequestHandler<UpdateTaskCommand, TaskModel>
        {
            private readonly ProjectManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, ProjectManagerContext context)
            {
                _context = context;
                _mapper = mapper;
            }


            public async Task<TaskModel> Handle(UpdateTaskCommand request, CancellationToken cancellationToken)
            {
                var task = await _context.Tasks.FindAsync(new object[] { request.Id }, cancellationToken)
                    ?? throw new KeyNotFoundException();

                _context.Entry(task).CurrentValues.SetValues(request);
                await _context.SaveChangesAsync(cancellationToken);
                return _mapper.Map<TaskModel>(task);
            }
        }
    }
}
