﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ProjectManager.Application.LinqQueries.Models;
using ProjectManager.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.LinqQueries.Queries
{
    public class GetTeamsWithParticipantsOlderThan10Query : IRequest<IEnumerable<TeamParticipantsModel>>
    {

        public class Handler : IRequestHandler<GetTeamsWithParticipantsOlderThan10Query, IEnumerable<TeamParticipantsModel>>
        {

            private readonly ProjectManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, ProjectManagerContext context)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<IEnumerable<TeamParticipantsModel>> Handle(GetTeamsWithParticipantsOlderThan10Query request, CancellationToken cancellationToken)
            {
                var teams = await _context.Teams
                                          .Where(t => t.Participants.All(u => DateTime.Now.Year - u.BirthDay.Year > 10))
                                          .ToListAsync(cancellationToken);

                var mappedTeams = _mapper.Map<IEnumerable<TeamParticipantsModel>>(teams);

                await Task.WhenAll(
                    mappedTeams.Select(team => Task.Run(
                        () => team.Participants = team.Participants.OrderByDescending(u => u.RegisteredAt).ToList(),
                        cancellationToken))
                    );

                return mappedTeams;
            }
        }
    }
}
