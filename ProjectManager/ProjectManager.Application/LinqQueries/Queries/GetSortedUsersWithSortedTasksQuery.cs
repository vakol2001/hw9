﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ProjectManager.Application.LinqQueries.Models;
using ProjectManager.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.LinqQueries.Queries
{
    public class GetSortedUsersWithSortedTasksQuery : IRequest<IEnumerable<UserTasksModel>>
    {
        public class Handler : IRequestHandler<GetSortedUsersWithSortedTasksQuery, IEnumerable<UserTasksModel>>
        {

            private readonly ProjectManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, ProjectManagerContext context)
            {
                _context = context;
                _mapper = mapper;
            }


            public async Task<IEnumerable<UserTasksModel>> Handle(GetSortedUsersWithSortedTasksQuery _, CancellationToken cancellationToken)
            {

                var users = await _context.Users.OrderBy(u => u.FirstName).ToListAsync(cancellationToken);
                var mappedUsers = _mapper.Map<IEnumerable<UserTasksModel>>(users);

                await Task.WhenAll(
                    mappedUsers.Select(user =>
                        Task.Run(() => user.Tasks = user.Tasks
                                                        .OrderByDescending(t => t.Name.Length)
                                                        .ToList(),
                                 cancellationToken)
                    ).ToList()
               );
                    
                return mappedUsers;
            }
        }
    }
}
