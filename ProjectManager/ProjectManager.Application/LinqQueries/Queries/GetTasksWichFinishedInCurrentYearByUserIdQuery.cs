﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ProjectManager.Application.LinqQueries.Models;
using ProjectManager.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.LinqQueries.Queries
{
    public class GetTasksWichFinishedInCurrentYearByUserIdQuery : IRequest<IEnumerable<TaskNameModel>>
    {
        public int Id { get; set; }

        public class Handler : IRequestHandler<GetTasksWichFinishedInCurrentYearByUserIdQuery, IEnumerable<TaskNameModel>>
        {

            private readonly ProjectManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, ProjectManagerContext context)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<IEnumerable<TaskNameModel>> Handle(GetTasksWichFinishedInCurrentYearByUserIdQuery request, CancellationToken cancellationToken)
            {
                var tasks = await _context.Tasks
                            .Where(t => t.PerformerId == request.Id)
                            .Where(t => t.FinishedAt.HasValue && t.FinishedAt.Value.Year == DateTime.Now.Year)
                            .ToListAsync(cancellationToken);

                return _mapper.Map<IEnumerable<TaskNameModel>>(tasks);
            }
        }
    }
}
