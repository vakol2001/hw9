﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using ProjectManager.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.LinqQueries.Queries
{
    public class GetProjectInfoByUserIdQuery : IRequest<IDictionary<string, int>>
    {
        public int Id { get; set; }

        public class Handler : IRequestHandler<GetProjectInfoByUserIdQuery, IDictionary<string, int>>
        {

            private readonly ProjectManagerContext _context;

            public Handler(ProjectManagerContext context)
            {
                _context = context;
            }

            public async Task<IDictionary<string, int>> Handle(GetProjectInfoByUserIdQuery request, CancellationToken cancellationToken)
            {
                return await _context.Projects
                                     .Where(p => p.AuthorId == request.Id)
                                     .ToDictionaryAsync(keySelector: p => $"Project # {p.Id} '{p.Name}'",
                                                        elementSelector: p => p.Tasks.Count,
                                                        cancellationToken: cancellationToken);
            }
        }
    }
}
