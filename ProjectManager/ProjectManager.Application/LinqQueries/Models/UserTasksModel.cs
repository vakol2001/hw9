﻿using ProjectManager.Application.Tasks.Models;
using System;
using System.Collections.Generic;

namespace ProjectManager.Application.LinqQueries.Models
{
    public class UserTasksModel
    {
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime BirthDay { get; set; }

        public IEnumerable<TaskModel> Tasks { get; set; }
    }
}
