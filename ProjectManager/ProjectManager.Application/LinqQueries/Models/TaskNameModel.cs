﻿namespace ProjectManager.Application.LinqQueries.Models
{
    public class TaskNameModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
