﻿using ProjectManager.Application.Users.Models;
using System.Collections.Generic;

namespace ProjectManager.Application.LinqQueries.Models
{
    public class TeamParticipantsModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<UserModel> Participants { get; set; }
    }
}
