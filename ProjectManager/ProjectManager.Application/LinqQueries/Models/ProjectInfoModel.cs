﻿using ProjectManager.Application.Projects.Models;
using ProjectManager.Application.Tasks.Models;

namespace ProjectManager.Application.LinqQueries.Models
{
    public class ProjectInfoModel
    {
        public ProjectModel Project { get; set; }
        public TaskModel LongestTaskByDescription { get; set; }
        public TaskModel ShortestTaskByName { get; set; }
        public int ParticipantsAmount { get; set; }
    }
}
