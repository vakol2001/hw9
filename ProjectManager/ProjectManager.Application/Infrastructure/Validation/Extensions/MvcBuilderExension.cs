﻿using Microsoft.Extensions.DependencyInjection;
using FluentValidation.AspNetCore;
using System.Reflection;

namespace ProjectManager.Application.Infrastructure.Validation.Extensions
{
    public static class MvcBuilderExension
    {
        public static IMvcBuilder AddCustomValidation(this IMvcBuilder builder)
        {
            builder.AddFluentValidation(conf =>
            {
                conf.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly());
            });
            return builder;
        }
    }
}
