﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reflection;

namespace ProjectManager.Application.Infrastructure.Mediator.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCustomMediator(this IServiceCollection services)
        {
            services.AddMediatR(Assembly.GetExecutingAssembly());
            return services;
        }
    }
}
