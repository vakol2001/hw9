﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reflection;

namespace ProjectManager.Application.Infrastructure.AutoMapper.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCustomAutoMapper(this IServiceCollection services)
        {
            if (services is null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            return services;
        }
    }
}
