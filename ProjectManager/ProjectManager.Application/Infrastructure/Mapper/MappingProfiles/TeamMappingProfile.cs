﻿using AutoMapper;
using ProjectManager.Application.LinqQueries.Models;
using ProjectManager.Application.Teams.Commands;
using ProjectManager.Application.Teams.Models;
using ProjectManager.Data.Entities;

namespace ProjectManager.Application.Infrastructure.Mapper.MappingProfiles
{
    public class TeamMappingProfile : Profile
    {
        public TeamMappingProfile()
        {
            CreateMap<Team, TeamModel>();
            CreateMap<CreateTeamCommand, Team>();
            CreateMap<UpdateTeamCommand, Team>();
            CreateMap<Team, TeamParticipantsModel>();
        }
    }
}
