﻿using MediatR;
using ProjectManager.Data;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Projects.Commands
{
    public class DeleteProjectCommand : IRequest
    {
        public int Id { get; set; }

        public class Handler : IRequestHandler<DeleteProjectCommand>
        {
            private readonly ProjectManagerContext _context;

            public Handler(ProjectManagerContext context)
            {
                _context = context;
            }


            public async Task<Unit> Handle(DeleteProjectCommand request, CancellationToken cancellationToken)
            {
                var project = await _context.Projects.FindAsync(new object[] { request.Id }, cancellationToken) 
                    ?? throw new KeyNotFoundException();
                _context.Projects.Remove(project);
                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}
